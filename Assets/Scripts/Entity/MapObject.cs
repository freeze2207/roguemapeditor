﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MapObjects", menuName = "My Assets/Map Objects")]
public class MapObject : ScriptableObject
{

    public List<PlaceableObject> list = new List<PlaceableObject>();

}
