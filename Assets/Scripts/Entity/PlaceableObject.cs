﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlaceableObject
{
    public enum Type
    {
        item = 0,
        environment,
        terrain,
        monster
    }
    public string m_name;
    public Type m_type;
    public Sprite m_sprite;
    public string m_textureName;
    public Vector2 m_rectPosition;
}
