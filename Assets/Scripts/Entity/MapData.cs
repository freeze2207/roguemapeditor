﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MapData", menuName = "My Assets/Map Data")]
public class MapData : ScriptableObject
{
    public string MapName;
    
    public PlaceableObject[] terrain_list = new PlaceableObject[100];
    public PlaceableObject[] item_list = new PlaceableObject[100];
}
