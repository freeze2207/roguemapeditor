using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor.UIElements;
using System.Collections.Generic;
using System.IO;

public class RogueMapEditor : EditorWindow
{
    private MapObject mapObjects;       // available placeables list

    private MapData mapData;            // rogue map data

    private Color transparent = new Color(0, 0, 0, 0);      // Transparent color constant
    private PlaceableObject emptyObject = new PlaceableObject();    //Empty object for delete
    
    private List<Image> mapDataTerrainImages = new List<Image>();       // Map terrain list
    private List<Image> mapDataItemImages = new List<Image>();          // Map item list

    // Edit mode and selected sprite to be paint, currently in this simple version you can get the mode and selection info in log.
    private int edit_mode = 0;          // 0 - none, 1 - Paint, 2 - Delete. Select the same mode will cancel the function

    private PlaceableObject selected_object;    // Select the same object will de-select the object

    private List<Image> m_objectsImgList = new List<Image>();           // placeable objects reference

    private PlaceableObject[] terrain_list_toSave = new PlaceableObject[100];       // temp terrain list for store
    private PlaceableObject[] item_list_toSave = new PlaceableObject[100];          // temp item list for store

    [MenuItem("Tools/Rogue Map Editor")]
    public static void ShowExample()
    {
        RogueMapEditor wnd = GetWindow<RogueMapEditor>();
        wnd.titleContent = new GUIContent("RogueMapEditor");
    }

    public void OnEnable()
    {
        RefreshEditor();
    }

    public void RefreshEditor()
    {
        rootVisualElement.Clear();
        VisualTreeAsset visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Scripts/Editor/RogueMapEditor.uxml");
        StyleSheet styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/Scripts/Editor/RogueMapEditor.uss");
        VisualElement rogueEditor = visualTree.Instantiate();
        rogueEditor.styleSheets.Add(styleSheet);
        rootVisualElement.Add(rogueEditor);

        ObjectField mapObjectSelector = rootVisualElement.Q<ObjectField>("MapObjectSelector");
        mapObjectSelector.objectType = typeof(MapObject);
        mapObjectSelector.RegisterCallback<ChangeEvent<Object>>(OnMapObjectsChanged);

        // Buttons set setup
        Button paint_button = rootVisualElement.Q<Button>("PaintButton");
        Button delete_button = rootVisualElement.Q<Button>("DeleteButton");
        Button save_button = rootVisualElement.Q<Button>("SaveButton");
        Button export_button = rootVisualElement.Q<Button>("ExportButton");
        paint_button.RegisterCallback<MouseUpEvent>(OnPaintClickEvent);
        delete_button.RegisterCallback<MouseUpEvent>(OnDeleteClickEvent);
        save_button.RegisterCallback<MouseUpEvent>(OnSaveClickEvent);
        export_button.RegisterCallback<MouseUpEvent>(OnExportClickEvent);

        ObjectField mapDataSelector = rootVisualElement.Q<ObjectField>("MapDataSelector");
        mapDataSelector.objectType = typeof(MapData);
        mapDataSelector.RegisterCallback<ChangeEvent<Object>>(OnMapDataChanged);

        // Map Grid init
        VisualElement grid = rootVisualElement.Q<VisualElement>("MapGrid");
        VisualElement right_panel = rootVisualElement.Q<VisualElement>("RightPanel");
        for (int i = 0; i < 10; i++)
        {
            VisualElement map_grid_panel = new VisualElement();
            map_grid_panel.style.flexDirection = FlexDirection.Row;
            right_panel.Add(map_grid_panel);

            for (int j = 0; j < 10; j++)
            {
                VisualElement cell = new VisualElement();
                cell.AddToClassList("grid_cell");
                map_grid_panel.Add(cell);

                // Terrain layer
                Box terrian_box = new Box();
                terrian_box.style.width = 50;
                terrian_box.style.height = 50;
                terrian_box.style.marginBottom = 3.0f;
                terrian_box.style.marginRight = 3.0f;
                cell.Add(terrian_box);

                Image terrain_image = new Image();
                terrain_image.name = "TerrianImage";
                terrain_image.sprite = null;      
                terrian_box.Add(terrain_image);
                mapDataTerrainImages.Add(terrain_image);

                // Item layer
                Box item_box = new Box();
                item_box.style.width = 50;
                item_box.style.height = 50;
                item_box.style.marginBottom = 3.0f;
                item_box.style.marginRight = 3.0f;
                item_box.style.backgroundColor = transparent;
                item_box.style.position = Position.Absolute;
                item_box.RegisterCallback<MouseUpEvent>(MapGridOnClickEvent);
                cell.Add(item_box);

                Image item_image = new Image();
                item_image.sprite = null;
                item_image.name = j + "," + i;      // Make the name contains the row and col info 
                item_box.Add(item_image);
                mapDataItemImages.Add(item_image);
            }
        }

    }

    private void OnMapObjectsChanged(ChangeEvent<Object> evt)
    {
        mapObjects = evt.newValue as MapObject;
        ScrollView scrollView = rootVisualElement.Q<ScrollView>("ObjectsList");
        if (mapObjects != null)
        {
            // Add Placeable items in the list
            for (int i = 0; i < mapObjects.list.Count; i++)
            {
                VisualElement image_row_panel = new VisualElement();
                image_row_panel.style.flexDirection = FlexDirection.Row;
                scrollView.Add(image_row_panel);

                Box box = new Box();
                box.style.width = 50;
                box.style.height = 50;
                box.style.marginBottom = 3;
                box.RegisterCallback<MouseUpEvent>(MapObjectOnClickEvent);
                image_row_panel.Add(box);

                Image image = new Image();
                image.style.flexShrink = 1.0f;
                image.sprite = mapObjects.list[i].m_sprite;
                image.name = i.ToString();
                box.Add(image);
                m_objectsImgList.Add(image);

                scrollView.Add(new Label(mapObjects.list[i].m_sprite.name));
            }
        }
        else
        {
            scrollView.Clear();
            m_objectsImgList.Clear();
        }
    }

    private void OnMapDataChanged(ChangeEvent<Object> evt)
    {
        mapData = evt.newValue as MapData;

        if (mapData != null)
        {
            // Read current data in the map data
            for (int i = 0; i < mapData.terrain_list.Length; i++)
            {
                mapDataTerrainImages[i].sprite = mapData.terrain_list[i].m_sprite;
                mapDataItemImages[i].sprite = mapData.item_list[i].m_sprite;

                terrain_list_toSave[i] = mapData.terrain_list[i];
                item_list_toSave[i] = mapData.item_list[i];
            }

        }
        else
        {
            // clear up map data and references
            foreach (Image image in mapDataTerrainImages)
            {
                image.sprite = null;
            }
            foreach (Image image in mapDataItemImages)
            {
                image.sprite = null;
            }
            for (int i = 0; i < 100; i++)
            {
                terrain_list_toSave[i] = null;
                item_list_toSave[i] = null;
            }
            
        }
    }

    private void MapObjectOnClickEvent(MouseUpEvent _event)
    {
        ToggleSelected(_event);
        Image image = _event.target as Image;

        // select and de-select item
        if (image != null)
        {
            if (selected_object != null && selected_object.m_sprite == image.sprite)
            {
                selected_object = null;
                VisualElement rightPanel = rootVisualElement.Q<VisualElement>("MidPanel");
                rightPanel.Q<Label>("ObjectName").text = "";
                rightPanel.Q<Label>("ObjectType").text = PlaceableObject.Type.environment.ToString();
            }
            else
            {
                selected_object = mapObjects.list[System.Convert.ToInt32(image.name)];
                // Store rect data
                Vector2 newPosition = selected_object.m_sprite.rect.position;
                newPosition.y = 576 - newPosition.y;
                selected_object.m_rectPosition = newPosition;
                
                // Store Texture name
                selected_object.m_textureName = Path.GetFileName(AssetDatabase.GetAssetPath(selected_object.m_sprite.texture));

                VisualElement rightPanel = rootVisualElement.Q<VisualElement>("MidPanel");
                rightPanel.Q<Label>("ObjectName").text = selected_object.m_name;
                rightPanel.Q<Label>("ObjectType").text = selected_object.m_type.ToString();
            }
        }

    }

    private void MapGridOnClickEvent(MouseUpEvent _event)
    {
        Image image = _event.target as Image;
        if(image == null)
        {
            image = ((Box)_event.target).Query().Children<Image>();
        }

        Vector2 position = GetPositionFromName(image.name);

        // Delete
        if (edit_mode == 2)
        {
            if (image.sprite != null)
            {
                image.sprite = null;
                item_list_toSave[(int)position.x + 10 * (int)position.y] = emptyObject;
            }
            else if(image.parent.parent.Q<Image>("TerrianImage").sprite != null)
            {
                image.parent.parent.Q<Image>("TerrianImage").sprite = null;
                terrain_list_toSave[(int)position.x + 10 * (int)position.y] = emptyObject;
            }

        }
        // Paint
        else if (edit_mode == 1 && selected_object != null)
        {
            if (selected_object.m_type == PlaceableObject.Type.terrain)
            {
                image.parent.parent.Q<Image>("TerrianImage").sprite = selected_object.m_sprite;
                terrain_list_toSave[(int)position.x + 10 * (int)position.y] = selected_object;
            }
            else
            {
                image.sprite = selected_object.m_sprite;
                item_list_toSave[(int)position.x + 10 * (int)position.y] = selected_object;
            }
        }
        
    }

    // Buttons click events 
    private void OnPaintClickEvent(MouseUpEvent _event)
    {
        ToggleSelected(_event);
        if (edit_mode != 1)
        {
            edit_mode = 1;
        }
        else
        {
            edit_mode = 0;
        }
        Debug.Log("Edit Mode: " + edit_mode);

    }

    private void OnDeleteClickEvent(MouseUpEvent _event)
    {
        ToggleSelected(_event);
        if (edit_mode != 2)
        {
            edit_mode = 2;
        }
        else
        {
            edit_mode = 0;
        }

        Debug.Log("Edit Mode: " + edit_mode);
    }

    private void OnSaveClickEvent(MouseUpEvent _event)
    {
        ToggleSelected(_event);
        edit_mode = 0;
        if (mapData != null)
        {
            mapData.terrain_list = terrain_list_toSave;
            mapData.item_list = item_list_toSave;

            /*MapData mp = ScriptableObject.CreateInstance<MapData>();
            mp.terrain_list = terrain_list_toSave;
            mp.item_list = item_list_toSave;
            mapData = mp;*/
            /*for (int i = 0; i < 100; i++)
            {
                mapData.terrain_list[i] = terrain_list_toSave[i];
                mapData.item_list[i] = item_list_toSave[i];
            }*/
        }
    }

    private void OnExportClickEvent(MouseUpEvent _event)
    {
        ToggleSelected(_event);
        edit_mode = 0;

        if (mapData != null)
        {
            
            string json = JsonUtility.ToJson(mapData);

            File.WriteAllText(Application.dataPath + "/RogueMap.json", json);
        }
    }

    // Selected effects
    private void ToggleSelected(MouseUpEvent _event)
    {
        VisualElement element = _event.target as VisualElement;
        if (element.ClassListContains("selected"))
        {
            element.RemoveFromClassList("selected");
        }
        else
        {
            List<Button> siblings = element.parent.Query().Children<Button>().ToList();
            if (siblings.Count == 0)
            {
                foreach (Image img in m_objectsImgList)
                {
                    img.RemoveFromClassList("selected");
                }
            }
            else
            {
                foreach (Button button in siblings)
                {
                    button.RemoveFromClassList("selected");
                }
            }
            element.AddToClassList("selected");
        }
    }

    // Retrive the postion data from image name
    private Vector2 GetPositionFromName(string _name)
    {
        return _name.Length == 3 ? new Vector2(_name[0] - 48, _name[2] - 48) : new Vector2(-1, -1);
    }
}